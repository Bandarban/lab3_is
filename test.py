import unittest
from main import great_func


class MyTestCase(unittest.TestCase):
    def test_great_func(self):
        self.assertEqual(great_func(2, 2), 4)


if __name__ == '__main__':
    unittest.main()
